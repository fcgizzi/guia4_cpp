//librerias
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

// archivo h de clase arbol
//constructores
#include "Arbol.h"

Arbol::Arbol() {
}
ofstream fp;

// función que recorre el arbol
void Arbol::recorrer(Nodo *nodo){
	// nodo debe ser distinto de NULL
	if (nodo != NULL){
		// si nodo apuntando a izquierda es distinto a NULL
		if (nodo-> izq != NULL){
			// de esta manera nodo apuntando al numero apuntará
			// al nodo apuntando a la izq apuntando al num
			fp <<  nodo-> info << "->" << nodo-> izq-> info << ";" << endl;
		}
		else{
			// sino nodo apuntando al número apuntará a la cadena
			string cadena = std::to_string(nodo-> info) + "i";
			fp << "\"" << cadena << "\"" << "[shape=point];" << endl;
			fp << nodo-> info << "->" << "\"" << cadena << "\"" << ";" << endl;
		}
		// si nodo apuntando a derecha es distinto a NULL
		if (nodo-> der != NULL){
			// se crea nodo apuntando a numero apuntando
			// a nodo apuntando a derecho apuntando a num
			fp << nodo-> info << "->" << nodo-> der-> info << ";" << endl;
		}
		else{
			// sino, apuntará a una cadena
			string cadena = std::to_string(nodo-> info) + "d´";
			fp << "\"" << cadena << "\"" << "[shape=point];" << endl;
			fp << nodo-> info << "->" << "\"" << cadena << "\"" << ";" << endl;
		}
		// luego vuelve a llamarse con nodo apuntando a izq y nodo apuntando a der
		recorrer(nodo-> izq);
		recorrer(nodo-> der); 
	}
}

// función que crea la imagen gráfica del arbol
void Arbol::crear_imagen(Nodo *nodo){
	// abre el archivo creado
	fp.open ("grafo.txt");
	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=pink];" << endl;
	// llama a recorrer el arbol
	recorrer(nodo);
	fp << "}" << endl;
	// cierra el archivo.
	fp.close();
  
	// genera un archivo txt y png del grafo del arbol
	system("dot -Tpng -ografo.png grafo.txt &");
  
	// muestra el grafo en pantalla
	system("eog grafo.png &");
}
          
