# Árboles Binarios

Se solicita un programa que cree un árbol binario de busqueda de elementos numéricos distintos entre si, también debe permitirse su manipulación.

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal

- Si no se tiene make, se usa `g++ main.cpp Arbol.cpp Grafo.cpp -o main` para compilar

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal

# Programa

El programa al inciarse solicita el ingreso de la raiz del Arbol.
Luego aparece un menú con las siguientes opciones:

1. Ingreso de elemento: Esta opción hará que el usuario ingrese un valor numérico, el cual, posteriormente se añadirá en el árbol. 
2. Eliminar elemento: Parecido a lo anterior, se solicita el número que se desea eliminar, este es buscado en el árbol y se elimina.
3. Modificar elemento: Esta opción usa las anteriores mencionadas, primeros pide elemento que se desea eliminar, se busca, se borra y posterior a esto se pide el dato por el cual se desea remplazar el dato anterior.
4. Mostrar recorrido de elementos: Esta opción abrirá un apartado con más opciones:
    Pregunta si quiere ver los datos del arbol en un tipo de recorrido:
    1. Preorden: Se ordena de manerea reversiva, primero el padre, luego el hijo izquierdo y al final el hijo derecho.
    2. Inorden: Se ordena empezando con el primero hijo izquierdo, luego el padre y finalmente el hijo derecho.
    3. Posorden: Se ordena de manera posterior, primero hijo izquierdo, luego el hijo derecho y finalmente el padre.
    Y agrega una opción para volver al menú
    4. Volver al menú: Opción para volver al menú.
5. Generar grafo: Esta opción hace que se generen dos archivos, un .txt y un .png, los cuales harán que se visualice una imagen gráfica del arbol que se ha estado creando.
6. Salir: Opcipin para salir del programa.

# Requisitos

- Sistema operativo Linux
- Make instalado
- Paquete graphviz. Se instala de la sigueinte manera: `sudo apt-get install graphviz` 

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi

