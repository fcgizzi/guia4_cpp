/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */

// librerías
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
using namespace std;

// clase
#include "Arbol.h"

// función que crea nodo
Nodo *crea_nodo(int dato) {
  Nodo *nodot;
  nodot = new Nodo();
  nodot-> izq = NULL;
  nodot-> der = NULL;
  nodot-> info = dato;
  return nodot;
}

// función encargada de recorrer e imprimir arbol en preorden
void arbol_preorden(Nodo *raiz){
	// si la raiz no es NULL, recorre en primero el padre
	// luego el hijo izquierdo y finalmente el hijo derecho
	if(raiz != NULL){
		cout << raiz-> info << " | ";
		arbol_preorden(raiz-> izq);
		arbol_preorden(raiz-> der);
	}
}

// función encargada de recorrer e imprimir arbol en inorden
void arbol_inorden(Nodo *raiz){
	// si la raiz es distina a NULL recorre primero el hijo izquierdo,
    //luego el padre y finalmente el hijo derecho
	if(raiz != NULL){
		arbol_inorden(raiz-> izq);
		cout << raiz-> info << " | ";
		arbol_inorden(raiz-> der);
	}
}

// función encargada de recorrer e imprimir arbol en posorden
void arbol_posorden(Nodo *raiz){
	// si la raiz no es NULL, recorre primero hijo izquierdo
	// luego el hijo derecho y finalmente el padre
	if(raiz != NULL){
		arbol_posorden(raiz-> izq);
		arbol_posorden(raiz-> der);
		cout << raiz-> info << " | ";
	}
} 

// función de insersión de nodos
void insertar_nodo(Nodo *nodo, int num){
    // el numero debe ser mayor que el node apuntando al número
    // luego se ve si el nodo a la derecha contiene información
    // si no contiene se crea un codo a la derecha
    // sino se llama a la función para que vuelva a revisar
	if (num > nodo-> info){
		if(nodo-> der == NULL){
			nodo-> der = crea_nodo(num);
		}
		else{
			insertar_nodo(nodo-> der, num);
		}
	}
	// de manera contraria
	// si el número es menor que el nodo apuntando al número
	// se revisa que e nodo izquierdo no contenga algún dato
	// si es así, se crea un nodo ahí
	// sino se vuelve a llamar a la función
	else if (num < nodo-> info){
			if(nodo-> izq == NULL){
				nodo-> izq = crea_nodo(num);
			}
			else{
			insertar_nodo(nodo-> izq, num);
			}
	}
	// sino, el número que se desea ingresar ya existe
	else{
		cout << "       >> El número que ud. desea ingresar ya existe <<" << endl;
	}
}

// función que encuentra el minimo
Nodo* minimo(Nodo* nodo){
	// se usa un nodo temporal
	Nodo* temp = nodo;
	// temporal y temporal apuntando a izq teienen que ser distinto de NULL
	while (temp && temp-> izq != NULL){
		temp = temp-> izq;
	}
	return temp;
}

// función encargada de eliminar nodos del arbol
Nodo* eliminar_nodo(Nodo *nodo, int num){
	// nodos temporales y auxiliares
	Nodo* temp;
	Nodo* aux;
	Nodo* aux1;
	bool bo;
 
	// si no hay datos en el notdo se retorna
	if(nodo == NULL){
		return nodo;
	}
	// si el número es menor  que el nodo del número
	// llama a la función con el nodo apuntado a la izquierda
	else if(num < nodo-> info){
		nodo-> izq = eliminar_nodo(nodo-> izq, num);
	}
	// si el número es mayor
	// llama a la función con el nodo apuntando a la derecha
	else if(num > nodo-> info){
		nodo-> der = eliminar_nodo(nodo-> der, num);
	}
	// si es igual entonces se debe eliminar
	else{
		// si los nodos derecho e izquierdo respectivamente son NULL
		// se elimina el nodo
		if(nodo-> der == NULL && nodo-> izq == NULL){
			delete nodo;
			nodo = NULL;
		}
    
		// Si no hay hijo a la derecha se iguala a variable temporal
		// y el nodo será el nodo temporal apuntando a la izquierda
		// luego se elimina el nodo temporal
		else if (nodo-> der == NULL){
			temp = nodo;
			nodo = temp-> izq;
			delete temp;
		}
		
		// Si no hay hijo a la izquierda se iguala a variable temporal
		// y el nodo será el nodo temporal apuntando a la derecha
		// luego se elimina el nodo temporal
		else if(nodo-> izq == NULL){
			temp = nodo;
			nodo = temp-> der;
			delete temp;
		}
		// si hay dos hijos, se busca el mínimo del nodo
		// el nodo apuntando al número se iguala a la temporal apuntando al número
		// ahí se elimina el nodo
		else{
			temp = minimo(nodo-> der);
			nodo-> info = temp-> info;
			nodo-> der = eliminar_nodo(nodo-> der, temp-> info);
       }
	}
	// retorna el nodo
	return  nodo;
}

// función encargada de modificar le valor elegido del arbol
void modificar_nodo(Nodo *nodo){
	// variables del número y el valor nuevo
	int num, nuevo;
	// se pide el dato a cambiar
	// se llama a la función de eliminación del dato
	// luego se ingresa el nuevo número y se llama a la función de ingreso
	cout << " >> Número que desee cambiar:"<< endl;
	cin >> num;
	eliminar_nodo(nodo, num);
	cout << " >> Ingrese el número con el que reemplazará al anterior " << endl;
	cin >> nuevo;
	insertar_nodo(nodo, nuevo);
}

//función menú de opciones
void menu(Nodo *raiz, Arbol arbol){
	// variables
	int op, op2, num;
	// se imprime un menú de opciones
	cout << "\n\t\t\t̉--MENU--" << endl;
	cout << "\t----------------------------------------" << endl;
    cout << "\t| 1 >> Ingresar elemento               |" << endl;
    cout << "\t| 2 >> Eliminar elemento               |" << endl;
    cout << "\t| 3 >> Modificar elemento              |" << endl;
    cout << "\t| 4 >> Mostrar contenido de árbol      |" << endl;
    cout << "\t| 5 >> Generar grafo                   |" << endl;
    cout << "\t| otro >> Salir                        | " << endl;
    cout << "\t----------------------------------------" << endl;
    cout << "\t >> ";
    cin >> op;
    system("clear");
    
    // primera opción de se llama a la función de insersión de nodo
    if(op == 1){
		// se pide ingresar un valor numérico
		cout << " >> Valor numérico que desea ingresar: " << endl;
		cin >> num;
        insertar_nodo(raiz, num);
        menu(raiz, arbol);
	}
	// opción 2 llama a función de eiliminación 
	else if(op == 2){
		cout << "Ingrese el numero: " << endl;
		cin >> num;
        eliminar_nodo(raiz, num);
        menu(raiz, arbol);
	}
	//opción 3 llama a la función de modificación de nodo
	else if(op == 3){
        // Se llama a modificar.
        modificar_nodo(raiz);
        menu(raiz, arbol);
	}
	// opción 4 mostrará los datos como se le solicite
	else if(op == 4){
		// imprime menu con opciones
		cout << "\n\t--------------------------------------" << endl;
		cout << "\t| >> Mostrar recorrido en:             |" << endl;
		cout << "\t| 1 >> Preorden                        |" << endl;
		cout << "\t| 2 >> Inorden                         |" << endl;
		cout << "\t| 3 >> Posorden                        |" << endl;
		cout << "\t| 4 >> Volver a atrás                  |" << endl;
		cout << "\t----------------------------------------" << endl;
		op2 = 0;
        while (op2 != 1 or 2 or 3 or 4){
			cout << "\t >> ";
			cin >> op2;
			system("clear");
			// cada opción llama a su función correspondiente
			//luego vuelve al menú
			if (op2 == 1){
				cout << " >> Elementos ordenados en preorden: " << endl;
				arbol_preorden(raiz);
				menu(raiz, arbol);
			}
			else if (op2 == 2){
				cout << " >> Elementos ordenados en inorden: " << endl;
				arbol_inorden(raiz);
				menu(raiz, arbol);
			}
			else if (op2 == 3){
				cout << " >> Elementos ordenados en posorden: " << endl;
				arbol_posorden(raiz);
				menu(raiz, arbol);
			}
			else if (op2 == 4){
				system("clear");
				menu(raiz, arbol);
			}
		}	
	}
	//opción 5 llama a una función de clase para crear el grafo
	else if(op == 5){
		arbol.crear_imagen(raiz);
		system("clear");
		menu(raiz, arbol);
	}
	// cualquier otro ingreso para salir
	else{
		cout << "ADIÓS" << endl;
	}   
}

// función principal
int main(){
    // variables raiz y arbol
    Nodo n, *raiz = NULL;
    Arbol arbol = Arbol();
    int i;
    cout << "        --Creación de Arbol Binario--" << endl;
    cout << "   Comenzaremos con el ingreso de la primera raiz" << endl;
    cout << " >> Introduzca una raiz de valor numérico: ";
    cin >> i;
    // se crea el nodo en bae a la raiz
	raiz = crea_nodo(i);
	system("clear");
	// limpia pantalla y se va al menú de opciones
	menu(raiz, arbol);
    return 0;
}

