// librerias
#include <fstream>
#include <iostream>
using namespace std;
// estrucutrador de programa principal
#include "main.h"

// clase de arbol
class Arbol {
  private:
  public:  
    // constructor
    Arbol();
    // funciones de la clase
    void recorrer(Nodo *p);
    void crear_imagen(Nodo *nodo);
};
